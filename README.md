Connor Campi

connor@campi.cc

prog2_1 is a class called Tokenizer, which creates objects which take strings and parse them into tokens based on the space character.

prog2_2 takes a file as a command line argument and passes it to a Tokenizer for it to be tokenized.

prog2_3 is a class called Parser, which takes in tokens and ensures they are part of a predetermined set of valid tokens.

prog2_4 takes a file as a command line argument, passing it through a Tokenizer first and a Parser second, then formatting and printing it to the screen.
