#include "prog2_1.hpp"
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char*argv[])
{
	cout << "Assignment #2-2, Connor Campi, connor@campi.cc" << endl;
	ifstream input;
	int line = 1;
	string current;
	Tokenizer* tok = new Tokenizer();
	input.open(argv[1],std::ifstream::in);	 // tokenizes text file given at command line

	try {
		while(getline(input,current)) {
			tok->Tokenize(current);
			line++;
		}
	}
	catch(const std::runtime_error& e) {
		cout << "Error on line " << line << ": " << e.what() << endl;
		return 0;
	}
	
	vector<string> output;
	vector<string> ocurrent;
	string formatter;
	int currentLine = 1;

	while(currentLine < line) {  //outputs formatted tokens
		ocurrent = tok->GetTokens();
		formatter.append(ocurrent.at(0));
		for(int i = 1; i < ocurrent.size() - 1; i++) {
			formatter.append("," + ocurrent.at(i));
		}
		output.push_back(formatter);
		formatter = "";
		currentLine++;
	}
	
	for(int i = 0;i<output.size();i++) {
		cout << output.at(i) << endl;
	}
	
	return 1;

}
