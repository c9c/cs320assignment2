#include "prog2_1.hpp"
#include "prog2_3.hpp"
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdexcept>
#include <string>

using namespace std;

int main(int argc,char* argv[]) {

	cout << "Assignment #2-4, Connor Campi, connor@campi.cc" << endl;
	ifstream input;
	int line = 1;
	string current;
	Tokenizer* tok = new Tokenizer();
	input.open(argv[1],std::ifstream::in);   // opens file given at command line
	try {
		while(getline(input,current)) {   // attempts to tokenize
			tok->Tokenize(current);
			line++;
		}
	}
	catch(const std::runtime_error& e) {
		cout << "Error on line " << line << ": " << e.what() << endl;
		return 0;
	}

	line = 1;
	vector<string> output;
	vector<string> pcurrent;
	string errout;
	Parser *parser = new Parser();

	while(tok->outputCounter < tok->tokens->size()) {  //attempts to parse tokens created after tokenization
		pcurrent = tok->GetTokens();
		if (parser->Parse(&pcurrent)) {
			if(pcurrent.size() == 2) {
				output.push_back(pcurrent.at(0));
			}

			if(pcurrent.size() == 3) {
				output.push_back(pcurrent.at(0) + "," + pcurrent.at(1));
			}
			line++;
		}
		else {
			for(int i = 0; i < pcurrent.size(); i++) {
				errout.append(pcurrent.at(i).c_str());
				if(i+1 < pcurrent.size()) {
					errout.append(" ");
				}
			}
			cout << "Parse error on line " << line << ": " << errout;
			return 0;				  //the wrong number of correct tokens causes parsing errors
		}
	}

	for(int i = 0;i<output.size();i++) {
		cout << output.at(i) << endl;
	}

}
